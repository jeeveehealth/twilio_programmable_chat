package twilio.flutter.twilio_programmable_chat.methods

import com.twilio.chat.*
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import twilio.flutter.twilio_programmable_chat.Mapper
import twilio.flutter.twilio_programmable_chat.PaginatorManager
import twilio.flutter.twilio_programmable_chat.TwilioProgrammableChatPlugin

object ChannelsMethods {
    fun createChannel(call: MethodCall, result: MethodChannel.Result) {
        val friendlyName = call.argument<String>("friendlyName")
            ?: return result.error("ERROR", "Missing 'friendlyName'", null)

        val channelTypeValue = call.argument<String>("channelType")
            ?: return result.error("ERROR", "Missing 'channelType'", null)

        val uniqueName = call.argument<String>("uniqueName")
            ?: return result.error("ERROR", "Missing 'uniqueName'", null)

        val userIdentities = call.argument<List<String>>("userIdentities")
            ?: return result.error("Error", "Missing 'userIdentities'", null)

        val channelType = when (channelTypeValue) {
            "PRIVATE" -> Channel.ChannelType.PRIVATE
            "PUBLIC" -> Channel.ChannelType.PUBLIC
            else -> null
        } ?: return result.error("ERROR", "Wrong value for 'channelType'", null)

        try {
            // TwilioProgrammableChatPlugin.chatClient?.channels?.createChannel(friendlyName, channelType, object : CallbackListener<Channel>() {
            //     override fun onSuccess(newChannel: Channel) {
            //         TwilioProgrammableChatPlugin.debug("ChannelsMethods.createChannel => onSuccess")
            //         result.success(Mapper.channelToMap(newChannel))
            //     }
            //
            //     override fun onError(errorInfo: ErrorInfo) {
            //         TwilioProgrammableChatPlugin.debug("ChannelsMethods.createChannel => onError: $errorInfo")
            //         result.error("${errorInfo.code}", errorInfo.message, errorInfo.status)
            //     }
            // })


            TwilioProgrammableChatPlugin.chatClient?.channels?.channelBuilder()
                ?.withUniqueName(uniqueName)
                ?.withFriendlyName(friendlyName)
                ?.withType(channelType)
                ?.build(object : CallbackListener<Channel>() {
                    override fun onSuccess(channel: Channel) {
                        channel.join(object : StatusListener() {
                            override fun onSuccess() {
                                for (userIdentity in userIdentities) {
                                    channel.members.addByIdentity(userIdentity, object : StatusListener() {

                                        override fun onSuccess() {
                                        }

                                        override fun onError(errorInfo: ErrorInfo?) {
                                            TwilioProgrammableChatPlugin.debug("${errorInfo?.code.toString()}, ${errorInfo?.message}")
                                        }

                                    })
                                }

                                TwilioProgrammableChatPlugin.debug("ChannelsMethods.createChannel => onSuccess")
                                return result.success(Mapper.channelToMap(channel))
                            }

                            override fun onError(errorInfo: ErrorInfo?) {
                                // return result.error(errorInfo?.code.toString(), errorInfo?.message, null)
                                return result.error("${errorInfo?.code}", errorInfo?.message, errorInfo?.status)
                            }
                        })
                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        TwilioProgrammableChatPlugin.debug("Error => ${errorInfo?.code} ${errorInfo?.message}")
                        return result.error(errorInfo?.code.toString(), errorInfo?.message, "")
                    }
                })

        } catch (err: IllegalArgumentException) {
            return result.error("IllegalArgumentException", err.message, null)
        }
    }

    fun getChannel(call: MethodCall, result: MethodChannel.Result) {
        val channelSidOrUniqueName = call.argument<String>("channelSidOrUniqueName")
            ?: return result.error("ERROR", "Missing 'channelSidOrUniqueName'", null)

        TwilioProgrammableChatPlugin.chatClient?.channels?.getChannel(channelSidOrUniqueName, object : CallbackListener<Channel>() {
            override fun onSuccess(newChannel: Channel) {
                TwilioProgrammableChatPlugin.debug("ChannelsMethods.getChannel => onSuccess")
                result.success(Mapper.channelToMap(newChannel))
            }

            override fun onError(errorInfo: ErrorInfo) {
                TwilioProgrammableChatPlugin.debug("ChannelsMethods.getChannel => onError: $errorInfo")
                result.error("${errorInfo.code}", errorInfo.message, errorInfo.status)
            }
        })
    }

    fun getPublicChannelsList(call: MethodCall, result: MethodChannel.Result) {
        TwilioProgrammableChatPlugin.chatClient?.channels?.getPublicChannelsList(object : CallbackListener<Paginator<ChannelDescriptor>>() {
            override fun onSuccess(paginator: Paginator<ChannelDescriptor>) {
                TwilioProgrammableChatPlugin.debug("ChannelsMethods.getPublicChannelsList => onSuccess")
                val pageId = PaginatorManager.setPaginator(paginator)
                result.success(Mapper.paginatorToMap(pageId, paginator, "channelDescriptor"))
            }

            override fun onError(errorInfo: ErrorInfo) {
                TwilioProgrammableChatPlugin.debug("ChannelsMethods.getPublicChannelsList => onError: $errorInfo")
                result.error("${errorInfo.code}", errorInfo.message, errorInfo.status)
            }
        })
    }

    fun getUserChannelsList(call: MethodCall, result: MethodChannel.Result) {
        TwilioProgrammableChatPlugin.chatClient?.channels?.getUserChannelsList(object : CallbackListener<Paginator<ChannelDescriptor>>() {
            override fun onSuccess(paginator: Paginator<ChannelDescriptor>) {
                TwilioProgrammableChatPlugin.debug("ChannelsMethods.getUserChannelsList => onSuccess")
                val pageId = PaginatorManager.setPaginator(paginator)
                result.success(Mapper.paginatorToMap(pageId, paginator, "channelDescriptor"))
            }

            override fun onError(errorInfo: ErrorInfo) {
                TwilioProgrammableChatPlugin.debug("ChannelsMethods.getUserChannelsList => onError: $errorInfo")
                result.error("${errorInfo.code}", errorInfo.message, errorInfo.status)
            }
        })
    }

    fun getMembersByIdentity(call: MethodCall, result: MethodChannel.Result) {
        val identity = call.argument<String>("identity")
            ?: return result.error("ERROR", "Missing 'identity'", null)

        val memberList = TwilioProgrammableChatPlugin.chatClient?.channels?.getMembersByIdentity(identity)
        val membersListMap = memberList?.map { Mapper.memberToMap(it) }
        result.success(membersListMap)
    }
}
